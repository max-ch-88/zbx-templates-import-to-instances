docker-run:
	docker network create zabbix-net || :
	docker run \
		-d \
		--name zabbix-db \
		--env="MYSQL_USER=zabbix" \
		--env="MYSQL_PASSWORD=zabbix" \
		--env="MYSQL_ROOT_USER=root" \
		--env="MYSQL_ROOT_PASSWORD=root_pwd" \
		--env="MYSQL_DATABASE=zabbix" \
		--net=zabbix-net \
		mysql:8.0-oracle \
		--character-set-server=utf8 \
		--collation-server=utf8_bin \
		--default-authentication-plugin=mysql_native_password || :
	sleep 5
	docker run \
		-d \
		--name zabbix-server \
		--env="DB_SERVER_HOST=zabbix-db" \
		--env="MYSQL_USER=zabbix" \
		--env="MYSQL_PASSWORD=zabbix" \
		--link=zabbix-db:zabbix-db \
		--net=zabbix-net \
		-p 10051:10051 \
		zabbix/zabbix-server-mysql:5.0.21-alpine || :
	sleep 5
	docker run \
		-d \
		--name zabbix \
		--env="ZBX_SERVER_HOST=zabbix-server" \
		--env="DB_SERVER_HOST=zabbix-db" \
		--env="MYSQL_USER=zabbix" \
		--env="MYSQL_PASSWORD=zabbix" \
		--link=zabbix-db:zabbix-db \
		--link=zabbix-server:zabbix-server \
		--net=zabbix-net \
		-p 8080:8080 \
		zabbix/zabbix-web-nginx-mysql:5.0.21-alpine || :

docker-clean:
	docker rm -f zabbix-db zabbix-server zabbix
	docker network rm zabbix-net

.PHONY: docker-run docker-clean
